﻿using SetCRMHelper.MayaModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SetCRMHelper
{
    public interface ISetCrmHelper
    {
        Task<RecordResponse> UpdateRecordAsync(RecordRequestParameters input);
    }
}
